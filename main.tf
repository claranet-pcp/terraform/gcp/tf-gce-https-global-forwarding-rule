resource "google_compute_global_forwarding_rule" "global_forwarder" {
  name       = "global-forwarder-${var.service}"
  ip_address = "${var.ip_address}"
  target     = "${google_compute_target_https_proxy.proxy.self_link}"
  port_range = 443
}

resource "google_compute_target_https_proxy" "proxy" {
  name             = "https-proxy-${var.service}"
  url_map          = "${google_compute_url_map.url_map.self_link}"
  ssl_certificates = ["${split(",", var.ssl_cert)}"]
}

resource "google_compute_url_map" "url_map" {
  name            = "url-map-${var.service}"
  default_service = "${var.default_backend}"
}
