output "forwarder_link" {
  value = "${google_compute_global_forwarding_rule.global_forwarder.self_link}"
}
output "ip_address" {
  value = "${google_compute_global_forwarding_rule.global_forwarder.ip_address}"
}
output "proxy_link" {
  value = "${google_compute_target_https_proxy.proxy.self_link}"
}
output "proxy_id" {
  value = "${google_compute_target_https_proxy.proxy.id}"
}
output "url_id" {
  value = "${google_compute_url_map.url_map.id}"
}
output "url_link" {
  value = "${google_compute_url_map.url_map.self_link}"
}
