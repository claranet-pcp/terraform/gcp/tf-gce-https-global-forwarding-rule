variable "service" {
  description = "The name for the service this https forwarding rule will serve, is the suffix for the name attribute on resources created by this module"
  type="string"
}

variable "default_backend" {
  description = "The backend service or backend bucket to use when none of the given rules match"
  type="string"
}

variable "ip_address" {
  description = "The IP address to use, if left blank an ephemeral IP is used"
  type="string"
}

variable "ssl_cert" {
  description = "Comma seperated list of the URLs of SSL Certificate resources that authenticate connections between users and load balancing."
  type="string"
}
